Title: Chess
Date: 2022-01-24 10:00
Category: Games
Summary: Chess is the first game available through Judge Hilda.

Chess is the first game available through me, Judge Hilda. You can launch a new game
easily by sending me an e-mail to *chess@playbymail.org* using the NEWGAME command.

Chess application is based on the powerful library
[python-chess](https://python-chess.readthedocs.io). If you find a bug you can report it
to me by sending an e-mail to *bug@playbymail.org*.

Comands to use are:

 * NEWGAME [*address*]: This command starts a new game between the sender of the e-mail
    and the one specified in the *address* parameter. White player is choosen by
    shuffling players.
 * GAME [*game_id*]: This command load a game based on the *game_id* identifier. It is
    mandatory in order to play.
 * PLAY [*movement*]: This command moves a piece on the board. *movement* can be
    described with an _UCI_ or _SAN_ string (e4, e2e4, e2-e4, etc.).
