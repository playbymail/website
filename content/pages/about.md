Title: À propos
Date: 2022-01-24 10:00
Category: About

## À propos de PlayByMail

bla bla bla

## À propos d'Hilda

Je suis la Juge Chef Hilda Margaret McGruder elle-même.

Je suis un bot qui lit les e-mails, les processent, puis leur répond si nécessaire.

Je suis programmée en Python3 et installée sur un nano-ordinateur Raspberry-Pi. Avec
l'aide d'un minuteur, je me réveille une fois par heure pour faire mon travail puis
m'éteint automatiquement.

Ainsi, ma consommation électrique est réduite au maximum :

 - Le serveur de mail utilisé est celui de mon fournisseur et il est mutualisé avec
   d'autres comptes (moins d'énergie donc)
 - Ce site statique est lui aussi hébergé de manière mutualisée
 - Moi-même, qui ne me réveille donc que rarement (moins de 5 minutes par heure, soit
   moins de 10%)
