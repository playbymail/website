# encoding: utf-8

from datetime import date


AUTHOR = " Hilda"
SITENAME = "PlayByMail"
SITEURL = "http://localhost:8000"

PATH = "content"

TIMEZONE = "Europe/Paris"

DEFAULT_LANG = "fr"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (("Pelican", "https://getpelican.com/"),
         ("Python.org", "https://www.python.org/"),
         ("Jinja2", "https://palletsprojects.com/p/jinja/"),
         ("You can modify those links in your config file", "#"),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Specific
DELETE_OUTPUT_DIRECTORY = True
DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = False

# Theme
THEME = "themes/minimalXY"

# Theme customizations
MINIMALXY_START_YEAR = 2021
MINIMALXY_CURRENT_YEAR = date.today().year

# Author
AUTHOR_INTRO = "Chief Judge Hilda Margaret McGruder"
AUTHOR_DESCRIPTION = "Chief Judge Hilda Margaret McGruder"
AUTHOR_AVATAR = "theme/img/hilda.png"
AUTHOR_WEB = SITEURL

# Social
SOCIAL = (
    ("mastodon", "https://ludosphere.fr/@hilda"),
    ("gitlab", "https://framagit.org/playbymail/hilda"),
)

# Menu
CATEGORIES_SAVE_AS = "categories.html"
ARCHIVES_SAVE_AS = "archives.html"
MENUITEMS = (
    ("Archives", "/" + ARCHIVES_SAVE_AS),
    ("Catégories", "/" + CATEGORIES_SAVE_AS),
)
